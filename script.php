<?php
  include('phpquery-onefile.php');
  include('array.php');
  $file = 'output-products.csv';
  $elements = array(
                'text' => array(
                  'model' => '#prod_code'
                  ),
                'html' => array(
                  'breadcrumbs'     => '#bread > li',
                  'main_image'      => '#prod_det_main_img',
                  'additionaimages' => '#prod_det_imgs > ul',
                  ),
              );
  $i = 0;

  $output = array();
  foreach ($array as $page) {
    $output[$i]["original_url"] = $page;
    phpQuery::newDocumentFileHTML($page);
    foreach ($elements as $type => $element) {
      foreach ($element as $col => $el) {
        $output[$i][$col] = getContent($el, $type);
      }
    }
    $i++;
    if ($i === 10) { break; }
    $percentage = ($i * 100)/773;
    // $right = 
    echo $i. "/773 - " . (int)$percentage . '%' . PHP_EOL;
  }

  echo "Formatting output..." . PHP_EOL;
  $formated_output = array();

  foreach ($output as $no => $out) {
    foreach ($out as $key => $o) {
      if (!empty($o)) {
        switch ($key) {
          case 'model':
              $model = $o;
              // $model = str_replace("Product Code: ", '', $o);
              // $model = str_replace(")", "", $model);
              $formated_output[$no][$key] = $model;
            break;
          
          case 'breadcrumbs':
              $breadcrumbs = $o;
              $breadcrumbs = strip_tags($o, '<span></span>');
              $breadcrumbs = preg_replace("/<\/span><span>/", "///", $breadcrumbs);
              $breadcrumbs = preg_replace("/<\/span><span itemprop=\"title\">/", "///", $breadcrumbs);
              $breadcrumbs = strip_tags($breadcrumbs);
              $breadcrumbs = rtrim(preg_replace("/^\nHome\/\/\/Stock\sList\/\/\//", "", $breadcrumbs));
              $formated_output[$no][$key] = addslashes($breadcrumbs);
            break;
          
          case 'main_image':
              $main_image = $o;
              $doc = new DOMDocument();
              $doc->loadHTML($o);
              $xpath = new DOMXPath($doc);
              $main_image = $xpath->evaluate("string(//img/@src)");
              $formated_output[$no][$key] = addslashes($main_image);
            break;
          
          case 'additionaimages':
              $additional_image = $o;
              $additional_image = strip_tags($o, "<img>");
              $additional_image = preg_replace("/<img\ssrc=\"/", ":::", $additional_image);
              $additional_image = preg_replace("/\" alt=\"\">/", "", $additional_image);
              $additional_image = trim(preg_replace('/\s+/', '', $additional_image));
              $additional_image = preg_replace('/^:::/', '', $additional_image);
              $formated_output[$no][$key] = addslashes($additional_image);
            break;
          
          default:
            break;
        }
      }
    }
  }

  $fileContent = "model;image;additional_images;category;original_url". "\r\n" ;
  foreach ($formated_output as $key => $entry) {
    $model        = isset($entry['model']) ? $entry['model'] : '';
    $image        = isset($entry['image']) ? $entry['image'] : '';
    $moreImg      = isset($entry['additionaimages']) ? $entry['additionaimages'] : '';
    $category     = isset($entry['breadcrumbs']) ? $entry['breadcrumbs'] : '';
    $original_url = isset($entry['original_url']) ? $entry['original_url'] : '';
    $fileContent .= "\"$model\";\"$image\";\"$moreImg\";\"$category\";\"$original_url\"\r\n";
  }
  // var_dump($formated_output);
  // echo "<br>>--------------------------<<br>";
  // var_dump($output);
  file_put_contents($file, $fileContent);
  echo "Done" . PHP_EOL;
  function getContent($path, $type = 'text')
  {
    $element = pq($path);

    switch ($type) {
      case 'text':
        $return = $element->text();
        break;
      
      case 'html':
        $return = $element->html();
        break;

      default:
        $return = "Error wrong type";
        break;
    }
    
    return $return;
  }
  ?>